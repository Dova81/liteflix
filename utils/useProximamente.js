import { useEffect, useState, useMemo, useCallback } from "react";
import API from "api-client/API";


export default function useProximamente() {

    const [peliculas, setPelicula] = useState([]);

    const currentPageMovies = useMemo(() => {
        return peliculas?.results?.slice(0, 4)
    }
        , [peliculas,])

    useEffect(() => {
        API.getProximamente()
            .then(resData => setPelicula(resData))
    }, []);

    return [currentPageMovies]
}