import React from 'react';
import styled from 'styled-components';
import useProximamente from 'utils/useProximamente';
import Play from 'public/play.svg'
import Add from 'public/add-list.svg'
import media from "styled-media-query";


const InfoCard = styled.div`
    display:grid;
    grid-template-columns: 100%;
    height: 155px;
    background-color: #000000;
    color: #ffffff;
    box-shadow:none;
    visibility:hidden;
    opacity:0;
    transition:visibility 0.7s cubic-bezier(0, 1.63, 1, 1.01),opacity 0.7s cubic-bezier(0, 1.63, 1, 1.01);
`

const ThumbsUp = styled.img`
    padding: 5px 7px;
`

const ThumbsWrapper = styled.div`
        border-radius: 50%;
        border-color:white;
        border-style: solid;
        border-width: 1px;
`;

const MovieContainer = styled.div.attrs(props => ({
    img: props.img,
}))`
    background-image: url(${props => props.img});
    background-image: linear-gradient(to top, rgba(0, 0, 0, 0.2) 77%, #000000), url(${props => props.img}); 
    background-color: #cccccc; 
    width: 255px;
    height: 155px;
    background-position: center;
    background-repeat: no-repeat; 
    background-size: cover; 
    color:#ffffff;
    display:grid;

    margin: 10px 15px;

    &:hover ${InfoCard}{
        visibility:visible;
        opacity:0.6;
    }

    ${media.lessThan("medium")`
        width:90%;
    `}
    
`;

const Container = styled.div`
    display: flex;
    margin-bottom: 5%;
    flex-direction: row;

    ${media.lessThan("medium")`
        flex-direction: column;
    `}
`

const ProxText = styled.div`
  width: 121px;
  height: 24px;
  font-family: "Montserrat";
  font-size: 20px;
  font-weight: bold;
  padding-bottom:10px;
`



const SocialMediaIcons = styled.div`
    margin-top:5px;
    margin-right:5px;
    display: flex;
    flex-direction: row-reverse;
`

const StyledAdd = styled(Add)`
    margin-left:10px;
`

const PlayButtonContainer = styled.div`
    display:flex;
    margin: 0px auto;
`

const PlayButtonStyled = styled(Play)`
  width: 40px;
  height: 40px;
`

const MovieInfo = styled.div`
  font-family: "Montserrat";
  font-size: 12px;
  margin-left:3%;
`

export default function Upcoming() {
    const [proximas] = useProximamente();

    return (
        <>
            <ProxText>
                Proximamente
            </ProxText>
            <Container>
                {proximas?.map((movie) =>
                    <MovieContainer key={movie.id} img={`https://image.tmdb.org/t/p/original${movie?.backdrop_path}`} >
                        <InfoCard>
                            <SocialMediaIcons>
                                <StyledAdd />
                                <ThumbsWrapper>
                                    <ThumbsUp src={'thumbs-up.png'} />
                                </ThumbsWrapper>
                            </SocialMediaIcons>
                            <PlayButtonContainer >
                                <PlayButtonStyled />
                            </PlayButtonContainer>
                            <MovieInfo>
                                <h4>{movie.title}</h4>
                                <p>
                                    {movie.vote_average * 10}% de votos positivos
                                </p>
                            </MovieInfo>
                        </InfoCard>
                    </MovieContainer>
                )

                }
            </Container>
        </>
    );
}