import React, { useState, useEffect } from 'react';
import usePeliculaDestacada from 'utils/usePeliculaDestacada';
import styled from 'styled-components';
import Header from 'components/Layout/Header';
import Featured from 'components/Layout/Featured';
import Upcoming from 'components/Layout/Upcoming';
import MyMovies from 'components/Layout/MyMovies';
import Popular from 'components/Layout/Popular';
import MovieContext from 'utils/movieContext';
import media from "styled-media-query";


const BaseImage = styled.div.attrs(props => ({
    img: props.img,
}))`
    background-image: url(${props => props.img}); 
    background-image: linear-gradient(to top, rgba(0, 0, 0, 0.2) 77%, #000000), url(${props => props.img}); 
    background-color: #000000; 
    height: 100vh; 
    background-position: center; 
    background-repeat: no-repeat; 
    background-size: cover; 
    color:#ffffff;
`;


const StyledDiv = styled.div`
    margin:0 10%;

    ${media.lessThan("medium")`
        margin-left:5%;
    `}
`

export default function Home() {
    const destacada = usePeliculaDestacada();
    const [movies, setMovies] = useState([]);
    const value = { movies, setMovies };

    useEffect(() => {
        const movies = JSON.parse(localStorage.getItem("Movies"))
        setMovies(movies)
    }, [])

    return (

        <MovieContext.Provider value={value}>
            <BaseImage img={`https://image.tmdb.org/t/p/original${destacada?.poster_path}`}>
                <StyledDiv>
                    <Header />
                    <Featured />
                    <Upcoming />
                    {movies?.length > 0 && <MyMovies movies={movies} />}
                    <Popular />
                </StyledDiv>
            </BaseImage>
        </MovieContext.Provider>

    );
}