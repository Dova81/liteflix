import React from 'react';
import styled from 'styled-components';
import Play from 'public/play.svg'
import Add from 'public/add-list.svg'
import media from "styled-media-query";



const InfoCard = styled.div`
    display:grid;
    grid-template-columns: 100%;
    width: 100%;
    height: 155px;
    background-color: #000000;
    color: #ffffff;
    box-shadow:none;

    visibility:hidden;
    opacity:0;
    transition:visibility 0.7s cubic-bezier(0, 1.63, 1, 1.01),opacity 0.7s cubic-bezier(0, 1.63, 1, 1.01);

`

const ThumbsUp = styled.img`
    padding: 5px 7px;
`

const ThumbsWrapper = styled.div`
        border-radius: 50%;
        border-color:white;
        border-style: solid;
        border-width: 1px;
`;

const MovieContainer = styled.div.attrs(props => ({
    img: props.img,
}))`
    background-image: url(${props => props.img}); /* fallback */
    background-image: linear-gradient(to top, rgba(0, 0, 0, 0.2) 77%, #000000), url(${props => props.img}); /* W3C */
    background-color: #cccccc; /* Used if the image is unavailable */
    width: 255px;
    height: 155px;
    background-position: center; /* Center the image */
    background-repeat: no-repeat; /* Do not repeat the image */
    background-size: cover; /* Resize the background image to cover the entire container */
    color:#ffffff;
    display:grid;
    
    margin: 10px 15px;

    &:hover ${InfoCard}{
        visibility:visible;
        opacity:0.6;
    }

    ${media.lessThan("medium")`
        width:90%;
    `}
`;

const Container = styled.div`
    display: flex;
    margin-bottom: 5%;
    flex-direction: row;

    ${media.lessThan("medium")`
        flex-direction: column;
    `}
`

const ProxText = styled.div`
  width: 121px;
  height: 24px;
  font-family: "Montserrat";
  font-size: 20px;
  font-weight: bold;
  font-stretch: normal;
  font-style: normal;
  line-height: normal;
  letter-spacing: normal;
  color: white;;
  padding-bottom:10px;
`


const SocialMediaIcons = styled.div`
    margin-top:5px;
    margin-right:5px;
    display: flex;
    flex-direction: row-reverse;
`

const StyledAdd = styled(Add)`
    margin-left:10px;
`

const PlayButtonContainer = styled.div`
    display:flex;
    margin: 0px auto;
`

const PlayButtonStyled = styled(Play)`
  width: 40px;
  height: 40px;
`

const MovieInfo = styled.div`
  font-family: "Montserrat";
  font-size: 12px;
  font-weight: normal;
  font-stretch: normal;
  font-style: normal;
  line-height: normal;
  letter-spacing: normal;
  margin-left:3%;
`

export default function MyMovies({ movies }) {

    return (
        <>
            <ProxText>
                Mi Lista
            </ProxText>
            <Container>
                {movies.slice(0, 4).map((movie) =>
                    <MovieContainer img={movie.img} >
                        <InfoCard>
                            <SocialMediaIcons>
                                <StyledAdd />
                                <ThumbsWrapper>
                                    <ThumbsUp src={'thumbs-up.png'} />
                                </ThumbsWrapper>
                            </SocialMediaIcons>
                            <PlayButtonContainer >
                                <PlayButtonStyled />
                            </PlayButtonContainer>
                            <MovieInfo>
                                <h4>{movie.name}</h4>
                                <p>
                                    {movie.category}
                                </p>
                            </MovieInfo>
                        </InfoCard>
                    </MovieContainer>
                )

                }

            </Container>
        </>
    );
}