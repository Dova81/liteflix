import Axios from "axios";


const server = process.env.HOST;

const api_key = process.env.API_KEY

const API = {
    getPeliculaDestacada: () => Axios.get(`${server}now_playing?api_key=${api_key}`).then(response => response.data),
    getProximamente: () => Axios.get(`${server}upcoming?api_key=${api_key}`).then(response => response.data),
    getPopulares: () => Axios.get(`${server}popular?api_key=${api_key}`).then(response => response.data),
};


export default API