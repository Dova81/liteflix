import React, { useState } from 'react';
import { useDropzone } from 'react-dropzone';
import styled from 'styled-components';
import ProgressBar from './ProgressBar';
import media from "styled-media-query";



const Container = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  padding: 20px;
  border-style: dashed;
  background-color: #fafafa;
  color: #bdbdbd;
  outline: none;
  height: 50px;
  border-radius: 10px;
  justify-content: center;

  p{
    font-family: "Montserrat";
    font-size: 16px;
    color: #9b9b9b;
    margin-left:10px;
    ${media.lessThan("medium")`
        font-size: 14px;
    `}
  }
`;

const ProgressBarWrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  padding: 20px;
  color: #bdbdbd;
  outline: none;
  height: 50px;
  background-color:#f3f3f3;
  border-radius: 10px;
`

const Cargado = styled.div`
  width: 88px;
  height: 15px;
  margin: 0 511.9px 10px 0;
  margin-right: auto;
  font-family: "Montserrat";
  font-size: 12px;
  font-weight: bold;
  font-stretch: normal;
  font-style: normal;
  line-height: normal;
  letter-spacing: normal;
  color: #000000;
  `


function StyledDropzone({ onComplete }) {
    const [progress, setProgress] = useState(0)

    const {
        acceptedFiles,
        getRootProps,
        getInputProps,
        isDragActive,
        isDragAccept,
        isDragReject
    } = useDropzone({
        accept: 'image/*',

        onDrop: async ([file]) => {
            var reader = new FileReader();
            reader.onload = function (e) {
                var contents = e.target.result;
                onComplete(contents);
            };
            reader.readAsDataURL(file);

            reader.onprogress = function (data) {
                if (data.lengthComputable) {
                    var progress = parseInt(((data.loaded / data.total) * 100), 10);
                    setProgress(progress)
                }
            }
        }
    });


    return (
        <>
            {progress > 0 ?
                <ProgressBarWrapper>
                    <Cargado> Cargado %{progress} </Cargado>
                    <ProgressBar completed={progress} />
                </ProgressBarWrapper>
                :
                <Container {...getRootProps({ isDragActive, isDragAccept, isDragReject })}>
                    <input {...getInputProps()} />
                    <img src="/clip.svg" />
                    <p> <b style={{ color: "blue" }}>Agregar archivo</b> o arrastrarlo y soltarlo aquí</p>
                </Container>
            }
        </>
    );
}

export default StyledDropzone;