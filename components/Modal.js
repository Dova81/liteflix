import React, { useCallback, useEffect, useState, useContext } from 'react';
import MovieContext from 'utils/movieContext';

import Dialog from '@material-ui/core/Dialog';
import styled from 'styled-components';
import media from "styled-media-query";

import Dropzone from 'components/Layout/Dropzone'
import Logo from 'public/liteflix.svg'
import Cancel from 'public/cancel.svg'

const Container = styled.div`
    display:flex;
    flex-direction: column;
    width: 730px;
    height: 354px;
    margin: auto;
    padding: 15px 14px 27px 35px;
    border-radius: 10px;
    background-color: white;

    ${media.lessThan("medium")`
        flex-direction: column;
        width: 245px;
    `} 
`

const Form = styled.div`
    display:flex;
    margin-top:5%;
    ${media.lessThan("medium")`
        flex-direction: column;
    `} 
`

const Name = styled.div`
    width:45%;
    display: flex;
    flex-direction: column;
    margin-right:5%;

    input{
        margin-top:17px;
        border:none !important;
        border-bottom:solid !important;
        border-width:1px !important;
        
        &:focus {
            outline: none;
        }
    }

    label{

        font-family: "Montserrat";
        font-size: 12px;
        font-weight: 500;
        font-stretch: normal;
        font-style: normal;
        line-height: normal;
        letter-spacing: 5px;
        color: #9b9b9b;
    }

    ${media.lessThan("medium")`
        width:90%;
        margin:5%;
    `} 
`;

const Select = styled.select`
    width: 100%;
    height: 35px;
    background: white;
    color: gray;
    padding-left: 5px;
    font-size: 14px;
    border: none;

    border:none !important;
    border-bottom:solid !important;
    border-width:1px !important;
    border-radius:1px;
    &:focus {
        outline: none;
    }
`;

const Category = styled.div`
    width:45%;
    display: flex;
    flex-direction: column;

    label{
        font-family: "Montserrat";
        font-size: 12px;
        color: #000000;
        font-size: 12px;
        font-weight: 500;
        font-stretch: normal;
        font-style: normal;
        line-height: normal;
        letter-spacing: 5px;
        color: #9b9b9b;
    }

    ${media.lessThan("medium")`
        width:90%;
        margin:5%;
    `} 
`

const StyledButton = styled.button`
  width: 350px;
  height: 70px;
  margin: auto;
  border-radius: 35px;
  background-color: #dedede;
  border:none;

  span{
    font-family: "Montserrat";
    font-size: 16px;
    color: white;
  }  

  ${media.lessThan("medium")`
        width: 150px;
        margin:5%;
    `} 
`

const ToastContainer = styled.div`
    display:flex;
    flex-direction: column;
    width: 730px;
    height: 354px;
    margin: auto;
    padding: 15px 14px 27px 35px;
    border-radius: 1px;
    background-color: #7ed321;

    h2,h4{
        margin-bottom:0;
        font-family: "Montserrat";
        font-weight: normal;
        font-stretch: normal;
        font-style: normal;
        line-height: normal;
        letter-spacing: normal;
        color: white;
    }

    ${media.lessThan("medium")`
        width: 245px;
        height: 275px;
    `} 
`

const CloseButton = styled.button`
    width: 192px;
    height: 70px;
    margin-top: auto;
    margin-right: auto;
    border: none;
    padding: 26px 71px 25px 72px;
    border-radius: 35px;
    background-color: #000000;
    color:white;
    font-family: "Montserrat";
    font-size: 16px;
`

const AlignCancel = styled.div`
    display:flex;
    justify-content: flex-end;
    margin:10px 0px;
    cursor: pointer;
`

const StyledLogo = styled(Logo)`
  width: 95px;
  height: 27px;

`

export default function Modal({ onClose, onAccept, open, }) {

    const [completedCharge, setCompletedCharge] = useState(false)
    const [movieName, setMovieName] = useState('')
    const [category, setCategory] = useState('')
    const [file, setFile] = useState('')
    const { movies, setMovies } = useContext(MovieContext);


    useEffect(() => {
        if (completedCharge) {
            setTimeout(() => onClose(), 2000)
        }
    }, [completedCharge])

    const onCompleteForm = useCallback(() => {
        setMovies([...movies, {
            name: movieName,
            category,
            img: file,
        }])

        localStorage.setItem("Movies", JSON.stringify([...movies, {
            name: movieName,
            category,
            img: file,
        }]))

        setCompletedCharge(true)
    }
        , [movieName, category, file])

    return (
        <Dialog maxWidth={"lg"} onBackdropClick={onClose} onClose={onClose} aria-labelledby="simple-dialog-title" open={open}>
            {completedCharge ?
                <ToastContainer>
                    <StyledLogo />
                    <h2> Felicitaciones! </h2>
                    <h4> {movieName} fue correcamente subido a la categoria {category} </h4>
                    <CloseButton>
                        Cerrar
                </CloseButton>
                </ToastContainer>
                :
                <Container>
                    <AlignCancel onClick={onClose}>
                        <Cancel />
                    </AlignCancel>
                    <Dropzone onComplete={(e) => setFile(e)} />
                    <Form>
                        <Name>
                            <label>NOMBRE DE LA PELICULA</label>
                            <input type="text" onChange={(evt) => setMovieName(evt.target.value)} />
                        </Name>
                        <Category>
                            <label>CATEGORIA</label>
                            <Select onChange={(evt) => setCategory(evt.target.value)}>
                                <option value="accion">Accion</option>
                                <option value="animacion">Animacion</option>
                                <option value="comedia">Comedia</option>
                                <option value="documentales">Documentales</option>
                            </Select>
                        </Category>
                    </Form>
                    <StyledButton onClick={onCompleteForm} >
                        <span>Subir pelicula</span>
                    </StyledButton>
                </Container>
            }
        </Dialog >
    );
}


