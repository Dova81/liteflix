import { useEffect, useState } from "react";
import API from "api-client/API";


export default function usePeliculaDestacada() {

    const [pelicula, setPelicula] = useState(null);

    useEffect(() => {
        API.getPeliculaDestacada()
            .then(resData => setPelicula(resData.results[0]))
    }, []);

    return pelicula;
}