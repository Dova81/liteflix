import React, { useState } from 'react';
import styled from 'styled-components';
import Bell from 'public/bell.svg'
import Modal from 'components/Modal';
import media from "styled-media-query";
import Fill from 'public/fill-orange.svg'


const Logo = styled.img`
  width: 95px;
  height: 27px;

  &.desktop{
      display:none;
  }  
`
const MenuItem = styled.div`
    font-size: 14px;
    margin:10px 0px;
`;


const StyledButton = styled.button`
    width:80%;
    height: 40px;
    margin: 0 0 0 19px;
    border-radius: 20px;
    background-color: #ff0000;
    color: #ffffff;
    box-shadow:none;
    font-family:"Montserrat";
    transition: width 0.2s cubic-bezier(0, 1.63, 1, 1.01);
    border:none;


`

const AlignText = styled.div`
    width: max-content;
    align-items: center;
    display: flex;
    margin: auto;
`

const PlusSign = styled.div`
  display:inline-block;
  width: 28px;
  height: 32px;
  
  background:
    linear-gradient(#fff,#fff),
    linear-gradient(#fff,#fff);
  background-position:center;
  background-size: 50% 2px,2px 50%; 
  background-repeat:no-repeat;
`

const Line = styled.span`
    display: block;
    width: 33px;
    height: 2px;
    margin-bottom: 5px;
    position: relative;
    
    background: #cdcdcd;
    border-radius: 3px;
`

const HamburgerMenu = styled.button`
    background-color:transparent;
    margin:0px;
    border:none;
`

const MobileWrapper = styled.div`
    margin-top:5%;
    display:flex;

    ${media.greaterThan("medium")`
        display:none;
    `}

`

const UserMenu = styled.div`
    display: flex;
    width:100%;
    flex-direction: column;
    align-items: flex-start; 
    align-content: stretch;
    flex-wrap: nowrap;
    margin: inherit;
    margin-top: 10%;

    hr{
        width:99%;
        background-color:#222222;
        border-width:0.5px
    }
`



const User = styled.div`
    margin:10px;
    display:flex;
    justify-content: space-around;
    align-items: center;
    width: 70%;
    height: 4%;
    padding: 6px 38px 6px 7px;
    border-radius: 18.5px;
    box-shadow: 0 0 10px 0 rgba(0, 0, 0, 0.1);
    background-color: #222222;
`

const Container = styled.div`
    text-align: center;
    color: white;
    align-items: center;
    font-family: "Montserrat";
    padding:15px;
    
    ${media.greaterThan("medium")`
        ${HamburgerMenu}{
                display:none;
            }
    `}

    ${media.lessThan("medium")`
        flex-direction:column;
        text-align: end;
        color: white;
        align-items: end;
        transition:  0.2s cubic-bezier(0, 1.63, 1, 1.01);

        ${MenuItem},
        ${Logo}, ${StyledButton},
        ${UserMenu},${User},.closed{
            display:none;
        }

        &.open{
        background-color:black;
        top: 0px;
        left: 0px;
        height: 100vh;
        z-index: 1;
        position: fixed;
        width:70%;

        ${MenuItem},
        ${Logo}, ${StyledButton},
        ${UserMenu},${User}{
            display:flex;
        }

        }
    `}
`


const UserIconContainer = styled.div`
    display:flex;
    width: 14px;
    height: 15px;
    padding: 5px 5.7px 5px 6px;
    background-color: #ce00ff;
    border-radius:100%;
`
const Name = styled.span`
  font-size: 12px;
`




export default function Header() {

    const [modal, setModal] = useState(false)
    const [displayMenu, setDisplayMenu] = useState(false)


    return (
        <>
            <MobileWrapper>
                <HamburgerMenu onClick={() => setDisplayMenu(!displayMenu)}>
                    <Line />
                    <Line />
                    <Line />
                </HamburgerMenu>
                <Logo src={"/liteflix.svg"} />
            </MobileWrapper>
            <Container className={displayMenu ? "open" : "closed"} >
                <Modal open={modal} onClose={() => setModal(false)} />
                <div style={{ display: 'flex' }}>
                    {displayMenu && <HamburgerMenu onClick={() => setDisplayMenu(!displayMenu)}>
                        <Line />
                        <Line />
                        <Line />
                    </HamburgerMenu>
                    }
                    <Logo src={"/liteflix.svg"} />
                </div>

                <User>
                    <UserIconContainer>
                        <Fill />
                    </UserIconContainer>
                    <Name>Ernesto Garmendia</Name>
                </User>
                <UserMenu>
                    Cambiar Usuario
                    <hr />
                    Configuracion
                    <hr />
                    Ayuda
                    <hr />
                </UserMenu>
                <MenuItem><Bell /> Novedades</MenuItem>
                <MenuItem>Series</MenuItem>
                <MenuItem>Peliculas</MenuItem>
                <MenuItem>Mi lista</MenuItem>
                <MenuItem>Niños</MenuItem>
                <StyledButton onClick={() => setModal(true)}>
                    <AlignText>
                        <PlusSign /> Agregar pelicula
                    </AlignText>
                </StyledButton>

                <MenuItem><b>Log out</b></MenuItem>
            </Container>
        </>
    );
}