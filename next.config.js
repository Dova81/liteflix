
module.exports = {
  webpack(config) {
    config.module.rules.push({
      test: /\.svg$/,
      use: ["@svgr/webpack"]
    });

    return config;
  },
  env: {
    API_KEY: process.env.API_KEY,
    HOSTNAME: process.env.HOSTNAME,
    PORT: process.env.PORT,
    HOST: process.env.HOST
  },
};