import React from 'react';
import styled from 'styled-components';
import usePopulares from 'utils/usePopulares';
import Play from 'public/play.svg'
import media from "styled-media-query";


const InfoCard = styled.div`
    display:flex;
    flex-direction: column;
    padding-left:10px;
    width: 97%;
    height: 507px;
    background-color: #000000;
    color: #ffffff;
    box-shadow:none;

    visibility:hidden;
    opacity:0;
    transition:visibility 0.7s cubic-bezier(0, 1.63, 1, 1.01),opacity 0.7s cubic-bezier(0, 1.63, 1, 1.01);

    ${media.lessThan("medium")`
        width: 95%;
        height: 328px;
    `}

`

const MovieContainer = styled.div.attrs(props => ({
    img: props.img,
}))`
    background-image: url(${props => props.img}); 
    background-image: linear-gradient(to top, rgba(0, 0, 0, 0.2) 77%, #000000), url(${props => props.img}); 
    background-color: #cccccc; 
    width: 255px;
    height: 507px;
    background-position: center; 
    background-repeat: no-repeat; 
    background-size: cover; 
    color:#ffffff;
    margin: 0px 15px;

    &:hover ${InfoCard}{
        visibility:visible;
        opacity:0.6;
    }

    ${media.lessThan("medium")`
        width: 90%;
        height: 328px;
    `}


`;


const Container = styled.div`
    display: flex;
    margin-bottom: 5%;
    flex-direction: row;
    ${media.lessThan("medium")`
        display:grid;
        grid-template-columns: 47% 50%;
        flex-direction: column;
    `}
`

const ProxText = styled.div`
  width: 100%;
  font-family: "Montserrat";
  font-size: 20px;
  font-weight: bold;
  font-stretch: normal;
  font-style: normal;
  line-height: normal;
  letter-spacing: normal;
  color: white;;
  padding-bottom:10px;
`

const PlayButtonStyled = styled(Play)`
    margin-bottom: 5px;
    margin-block-start: auto;
    width: 40px;
    height: 40px;
`

const StyledTitle = styled.div`
  font-family: "Montserrat";
  font-size: 18px;
  font-weight: bold;
`

const StyledBody = styled.p`
  font-family: "Montserrat";
  font-size: 8px;

  ${media.lessThan("medium")`
        display:none;
    `}
`

const ThumbsUp = styled.img`
    padding: 5px 7px;
`

const ThumbsWrapper = styled.div`
            margin: auto;
        border-radius: 50%;
        border-color:white;
        border-style: solid;
        border-width: 1px;
`;


export default function Popular() {
    const [proximas] = usePopulares();

    return (
        <>
            <ProxText>
                POPULARES DE LITEFLIX
            </ProxText>
            <Container>
                {proximas?.map((movie) =>
                    <MovieContainer key={movie.id} img={`https://image.tmdb.org/t/p/original${movie?.poster_path}`} >
                        <InfoCard>
                            <PlayButtonStyled />
                            <div style={{ display: "flex" }}>
                                <StyledTitle>{movie.title}</StyledTitle>
                                <ThumbsWrapper>
                                    <ThumbsUp src={'thumbs-up.png'} />
                                </ThumbsWrapper>
                            </div>
                            <StyledBody>
                                {movie.overview}
                            </StyledBody>
                        </InfoCard>
                    </MovieContainer>
                )
                }
            </Container>
        </>
    );
}