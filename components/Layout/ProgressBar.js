import React from 'react'
import styled from 'styled-components';

const ContainerStyles = styled.div`
        height: 20px;
        width: 100%;
        background-color: #e0e0de;
        border-radius: 10px;
        background-color: #7ed321;
`

const ProgressBar = (props) => {
    const { bgcolor, completed } = props;

    const fillerStyles = {
        height: '100%',
        width: `${completed}%`,
        backgroundColor: bgcolor,
        borderRadius: 'inherit',
        textAlign: 'right'
    }


    return (
        <ContainerStyles >
            <div style={fillerStyles}>
            </div>
        </ContainerStyles>
    );
};

export default ProgressBar;