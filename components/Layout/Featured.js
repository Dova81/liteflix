import usePeliculaDestacada from 'utils/usePeliculaDestacada';
import React from 'react';
import styled from 'styled-components';
import Play from 'public/play.svg'
import Add from 'public/add-list.svg'
import media from "styled-media-query";

const LiteflixOriginal = styled.div`
  margin-top:6%;
  text-align: left;
  font-family: "Montserrat";
  font-size: 31px;

  ${media.lessThan("medium")`
    margin-top:15%;
    margin-left:15%;
    font-size: 18px;
    `}
`

const MovieTitle = styled.div`
    font-family: "Roboto";
    font-size: 110px;
    font-weight:bold;
    ${media.lessThan("medium")`
        margin-top:15%;
        margin-left:15%;
        font-size: 72px;
    `}
`

const DarkButton = styled.button`
  width: 160px;
  height: 40px;
  margin: 19px 30px 25px 1px;
  opacity: 0.5;
  border-radius: 20px;
  background-color: #000000;
  color:#ffffff;
  display: flex;
  border:none;
  align-items: center;
  font-family: "Montserrat";

  ${media.lessThan("medium")`
        margin-right:40px;
        &.notMobile {
            display:none;
        }
    `}
  
`

const InteractButtons = styled.div`
     display: flex;
     align-items: center;
     svg{
        margin-right:10px;
    }
    ${media.lessThan("medium")`
        justify-content: flex-end;
    `}

`

const AlignLogo = styled.div`
    ${media.greaterThan("medium")`
        display:none;
    `}
`


const Overview = styled.p`
  width: 537px;
  height: 168px;
  font-family: "Montserrat";
  font-size: 18px;  
  ${media.lessThan("medium")`
        display:none;
    `}
`


export default function Featured() {
    const destacada = usePeliculaDestacada();
    return (
        <>
            <LiteflixOriginal>
                ORIGINAL DE <b>LITEFLIX</b>
            </LiteflixOriginal>
            <MovieTitle>
                {destacada?.original_title}
            </MovieTitle>
            <InteractButtons>
                <DarkButton>
                    <Play /> Reproducir
                </DarkButton>
                <AlignLogo>
                    <Add />
                </AlignLogo>
                <DarkButton className="notMobile">
                    <Add /> Mi Lista
                </DarkButton>
            </InteractButtons>
            <Overview>
                {destacada?.overview}
            </Overview>
        </>
    );
}