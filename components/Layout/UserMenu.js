import React from 'react';
import styled from 'styled-components';


const Container = styled.div`
    display: flex;
    width:100%;
    flex-direction: column;
    align-items: flex-start; 
    align-content: stretch;
    flex-wrap: nowrap;
    margin: inherit;
    margin-top: 10%;

    hr{
        width:99%;
        background-color:#222222;
        border-width:0.5px
    }
`


export default function UserMenu() {
    return (

        <Container>
            Configuracion
            <hr />
            Ayuda
            <hr />
            <b>Log Out </b>
            <hr />
        </Container>

    );
}