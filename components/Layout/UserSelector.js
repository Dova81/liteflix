import React from 'react';
import styled from 'styled-components';
import Fill from 'public/icono litebox.svg'
import Arrow from 'public/arrow.svg'
import UserMenu from 'components/Layout/UserMenu'

const UsersDisplay = styled.div`
    font-family:"Montserrat";
    visibility:hidden;
    opacity:0;
    position:absolute;
    width: 130px;
    height: 218px;
    padding: 11px 9px 11px 10px;
    border-radius: 5px;
    background-color: white;
    right: 13%;
    top: 40px;
`


const Container = styled.div`
    text-align: center;
    color: white;
    font-family: "Montserrat";
    display:flex;
    align-items: center;
    

    &:hover ${UsersDisplay}{
        visibility:visible;
        opacity:1;
        transition: visibility 0.4s cubic-bezier(0, 1.63, 1, 1.01),opacity 0.4s cubic-bezier(0, 1.63, 1, 1.01);
    }
`

const UserIconContainer = styled.div`
  width: 25px;
  height: 25px;
  margin: 10px 10px 7px 20px;
  padding: 5px 5.7px 5px 6px;
`

const TriangleTooltip = styled.span`
    content:'';
    display:block;
    width:0;
    height:0;
    position:absolute;
    border-left: 5px solid transparent;
    border-right: 5px solid transparent;
  
    border-bottom: 5px solid white;
    left: 114px;
    top: -5px;
`

const MenuWrapper = styled.div`
        display:flex;
        flex-direction: column;
        font-family: "Montserrat";
        font-size: 12px;
        color: #000000;
`

const UserOptions = styled.div`
    display: flex;
    flex-direction: column;
`

const User = styled.button`
    padding-right: 40px;
    width: 111px;
    height: 37px;
    color:#9b9b9b;
    background-color:#ffffff;
    border:none;

    :hover{
        color:#000000;
        border:solid;
        border-width:1px;
        border-radius: 10px;
        border-color: aliceblue;
    }
`

export default function UserSelector() {
    return (
        <Container>

            <UserIconContainer>
                <Fill />
            </UserIconContainer>
            <Arrow />
            <UsersDisplay>
                <TriangleTooltip />
                <MenuWrapper>
                    <UserOptions>
                        <User>
                            <Fill />
                        User 01
                        </User>
                        <User>
                            <Fill />
                        User 02
                        </User>
                        <User>
                            <Fill />
                        User 03
                        </User>
                    </UserOptions>
                    <UserMenu />
                </MenuWrapper>
            </UsersDisplay>
        </Container>
    );
}