import { useEffect, useState, useMemo, useCallback } from "react";
import API from "api-client/API";


export default function usePopulares() {

    const [populares, setPopulares] = useState([]);
    const [currentPage, setCurrentPage] = useState(0);

    const currentPageMovies = useMemo(() => {
        return populares?.results?.slice(currentPage * 4, currentPage * 4 + 4)
    }
        , [populares, currentPage])

    useEffect(() => {
        API.getPopulares()
            .then(resData => setPopulares(resData))
    }, []);

    return [currentPageMovies]
}