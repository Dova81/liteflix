import React, { useState } from 'react';
import styled from 'styled-components';
import Bell from 'public/bell.svg'
import UserSelector from 'components/Layout/UserSelector';
import MobileHeader from 'components/Layout/MobileHeader';
import Modal from 'components/Modal';
import media from "styled-media-query";


const Logo = styled.img`
  width: 95px;
  height: 27px;

  &.desktop{
      display:none;
  }  
`
const MenuItem = styled.div`
    font-size: 14px;
    font-weight: bold;
    margin: 15px;

`;


const ButtonTest = styled.span`
    visibility:hidden;
    opacity:0;
    
`

const StyledButton = styled.button`
    width: 40px;
    height: 40px;
    border-radius: 20px;
    background-color: #ff0000;
    color: #ffffff;
    box-shadow:none;
    transition: width 0.2s cubic-bezier(0, 1.63, 1, 1.01);
    border:none;
    font-family:"Montserrat";

    &:hover {
        position: relative;
        width:160px;
    }

    &:hover ${ButtonTest}{
        visibility:visible;
        opacity:1;
        transition: visibility 0.4s cubic-bezier(0, 1.63, 1, 1.01),opacity 0.4s cubic-bezier(0, 1.63, 1, 1.01);
    }

`

const StyledBell = styled(Bell)`
    margin-left:10px;
`

const AlignText = styled.div`
    width: max-content;
    align-items: center;
    display: flex;
    margin: auto;
`

const AccountMenu = styled.div`
    display:flex;
    align-items: center;
    margin-left: auto;
    margin-right: 5%;
    ${media.lessThan("medium")`
        display:none;
    `}
`


const PlusSign = styled.div`
  display:inline-block;
  width: 28px;
  height: 32px;
  
  background:
    linear-gradient(#fff,#fff),
    linear-gradient(#fff,#fff);
  background-position:center;
  background-size: 50% 2px,2px 50%; 
  background-repeat:no-repeat;
`


const Container = styled.div`
    text-align: center;
    color: white;
    align-items: center;
    font-family: "Montserrat";
    display:flex;

    ${media.lessThan("medium")`
        display:none;
    `} 
`

const MobileWrapper = styled.div`
    display:flex;
    ${media.greaterThan("medium")`
        display:none;
    `} 
`


export default function Header() {

    const [modal, setModal] = useState(false)


    return (
        <>
            <MobileWrapper>
                <MobileHeader />
            </MobileWrapper>
            <Container >
                <Modal open={modal} onClose={() => setModal(false)} />
                <Logo src={"/liteflix.svg"} />
                <MenuItem>Inicio</MenuItem>
                <MenuItem>Series</MenuItem>
                <MenuItem>Peliculas</MenuItem>
                <MenuItem>Agregados recientemente</MenuItem>
                <MenuItem>Mi lista</MenuItem>
                <StyledButton onClick={() => setModal(true)}>
                    <AlignText>
                        <PlusSign /> <ButtonTest>Agregar pelicula </ButtonTest>
                    </AlignText>
                </StyledButton>

                <AccountMenu>
                    Niños
                <StyledBell />
                    <UserSelector />
                </AccountMenu>
            </Container>
        </>
    );
}